"""footbal_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth.views import LoginView
from django.urls import path, include
from django.urls import include, re_path
from renting.forms import AuthenticationLoginForm
from djgeojson.views import GeoJSONLayerView
from renting.models import MushroomSpot
from django.views.generic import TemplateView

urlpatterns = [
    re_path(r'^data.geojson$', GeoJSONLayerView.as_view(model=MushroomSpot, properties=('title', 'description', 'picture_url')), name='data'),
    path('admin/', admin.site.urls),
    path('', include('renting.urls')),
    path('login/', LoginView.as_view(form_class=AuthenticationLoginForm), name='login'),
    path('', include('django.contrib.auth.urls')),
]
