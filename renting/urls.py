from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import LoginView
from django.urls import path, include
from djgeojson.views import GeoJSONLayerView


from renting import views
from renting.forms import AuthenticationLoginForm
from renting.models import MushroomSpot

urlpatterns = [
    path('', views.HomeTemplateView.as_view(), name='home'),
    path('create-user/', views.UserCreateView.as_view(), name='create_user'),
    path('login/', LoginView.as_view(form_class=AuthenticationLoginForm), name='login'),
    path('', include('django.contrib.auth.urls')),
    path('add-field/', views.AddFieldView.as_view(), name='add_field'),
    path('update-field/<int:pk>', views.UpdateFieldView.as_view(), name='update_field'),
    path('list-of-fields/', views.FieldListView.as_view(), name='list_of_fields'),
    path('delete-field/<int:pk>', views.FieldDeleteView.as_view(), name='delete_field'),
    path('data.geojson',
         GeoJSONLayerView.as_view(model=MushroomSpot, properties='title'), name='data')

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
