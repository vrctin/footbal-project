from django.contrib.auth.models import User
from django.db import models
from djgeojson.fields import PolygonField


# Create your models here.
class UserExtend(User):
    age = models.IntegerField(default=0)
    phone = models.CharField(max_length=10)

    def __str__(self):
        return f'{self.first_name} {self.last_name} '


class Field(models.Model):
    name = models.CharField(max_length=40)
    adress = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=10)
    price = models.IntegerField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.name}'


class MushroomSpot(models.Model):
    title = models.CharField(max_length=256)
    description = models.TextField()
    picture = models.ImageField()
    geom = PolygonField()

    def __str__(self):
        return self.title

    @property
    def picture_url(self):
        return self.picture.url
