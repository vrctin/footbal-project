from django.shortcuts import render, redirect

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import TemplateView, CreateView, UpdateView, ListView, DeleteView

from renting.forms import UserExtendForm, AddFieldForm, UpdateFieldForm
from renting.models import UserExtend, Field


class HomeTemplateView(TemplateView):
    template_name = 'home/home.html'


class UserCreateView(CreateView):
    template_name = 'userextend/create_user.html'
    model = UserExtend
    form_class = UserExtendForm
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        if form.is_valid() and not form.errors:
            new_user = form.save(commit=False)
            new_user.first_name = new_user.first_name.title()
            new_user.username = f'{new_user.email}'
            new_user.save()
        return redirect('login')


class AddFieldView(CreateView):
    template_name = 'fields/add_field.html'
    model = Field
    form_class = AddFieldForm
    success_url = reverse_lazy('home')


class UpdateFieldView(UpdateView):
    template_name = 'fields/update_field.html'
    model = Field
    form_class = UpdateFieldForm
    success_url = reverse_lazy('home')


class FieldListView(ListView):
    template_name = 'fields/list_of_fields.html'
    model = Field
    context_object_name = 'all_fields'

    # def get_queryset(self):
    #     return Field.objects.filter(active=True)


class FieldDeleteView(DeleteView):
    template_name = 'fields/delete_field.html'
    model = Field
    success_url = reverse_lazy('list_of_fields')
