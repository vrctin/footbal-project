from django import forms
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm

from django.forms import TextInput, EmailInput

from renting.models import UserExtend, Field


class AuthenticationLoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update(
            {'class': 'form-control', 'placeholder': 'Introduceti email-ul'})
        self.fields['password'].widget.attrs.update(
            {'class': 'form-control', 'placeholder': 'Introduceti parola'})


class UserExtendForm(UserCreationForm):
    class Meta:
        model = UserExtend
        fields = ['first_name', 'last_name', 'email', 'phone']
        widgets = {
            'first_name': TextInput(attrs={'placeholder': 'Introduceti-va prenumele', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Introduceti-va numele', 'class': 'form-control'}),
            'email': EmailInput(attrs={'placeholder': 'Introduceti-va email-ul', 'class': 'form-control'}),
            'phone': TextInput(attrs={'placeholder': 'Introduceti-va numarul de telefon', 'class': 'form-control'}),

        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['password1'].widget.attrs.update(
            {'class': 'form-control', 'placeholder': 'Introduceti parola'})
        self.fields['password2'].widget.attrs.update(
            {'class': 'form-control', 'placeholder': 'Confirmati parola'})


class AddFieldForm(forms.ModelForm):
    class Meta:
        model = Field
        fields = ['name', 'adress', 'phone_number', 'price']
        widgets = {
            'name': TextInput(attrs={'placeholder': 'Inserati numele terenului', 'class': 'form-control'}),
            'adress': TextInput(attrs={'placeholder': 'Inserati adresa', 'class': 'form-control'}),
            'phone_number': TextInput(
                attrs={'placeholder': 'Introduceti-va numarul de telefon', 'class': 'form-control'}),
            'price': TextInput(attrs={'placeholder': 'Inserati pretul pentru o ora', 'class': 'form-control'})
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        fields = Field.objects.all()
        for field in fields:
            if field.name == cleaned_data.get('name'):
                msg = 'Terenul a fost deja inregistrat'
                self._errors['adress'] = self.error_class([msg])

        return cleaned_data


class UpdateFieldForm(forms.ModelForm):
    class Meta:
        model = Field
        fields = ['name', 'adress', 'phone_number', 'price']
        widgets = {
            'name': TextInput(attrs={'placeholder': 'Inserati numele terenului', 'class': 'form-control'}),
            'adress': TextInput(attrs={'placeholder': 'Inserati adresa', 'class': 'form-control'}),
            'phone_number': TextInput(
                attrs={'placeholder': 'Introduceti-va numarul de telefon', 'class': 'form-control'}),
            'price': TextInput(attrs={'placeholder': 'Inserati pretul pentru o ora', 'class': 'form-control'})
        }
