from django.contrib import admin

# Register your models here.
from leaflet.admin import LeafletGeoAdmin
from . import models as mushrooms_models, models

admin.site.register(models.MushroomSpot, LeafletGeoAdmin)